#!/usr/bin/python3.4
# -*- coding: utf-8 -*-
import telebot
from time import sleep
import json
from telebot import types

token = 'TOKEN'
bot = telebot.TeleBot(token)


# Replies with chat_id
@bot.message_handler(func=lambda message: True, content_types=['text'])
def default_command(message):
    # Send chat_id if private chat
    if isinstance(message.chat, types.User):
        bot.send_message(message.chat.id, 'Your personal ID is {0!s}'.format(message.chat.id))
    # Send both chat_id and user's own id if group chat
    elif isinstance(message.chat, types.GroupChat):
        # Only if bot is explicitly mentioned
        if '@my_id_bot' in message.text:
            bot.send_message(message.chat.id, 'This group\'s ID is {0!s}\nYour personal ID is {1!s}'.format(message.chat.id, message.from_user.id))

#Returns the file_id of the following types:
#audio:
@bot.message_handler(func=lambda message: True, content_types=['audio'])
def get_audio_id(message):
    bot.reply_to(message, 'Your audio ID is:\n{0!s}'.format(message.audio.file_id))
#photo:
@bot.message_handler(func=lambda message: True, content_types=['photo'])
def get_audio_id(message):
    bot.reply_to(message, 'Your photo ID is:\n{0!s}'.format(message.photo[-1].file_id))
#sticker:
@bot.message_handler(func=lambda message: True, content_types=['sticker'])
def get_sticker_id(message):
    bot.reply_to(message, 'Your sticker ID is:\n{0!s}'.format(message.sticker.file_id))
#video:
@bot.message_handler(func=lambda message: True, content_types=['video'])
def get_sticker_id(message):
    bot.reply_to(message, 'Your video ID is:\n{0!s}'.format(message.video.file_id))

if __name__ == '__main__':
    bot.polling(none_stop=True)
    while True:
        sleep(300)
