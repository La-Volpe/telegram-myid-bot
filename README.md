## "What's my Telegram ID?" bot ##

This bot sends you your chat ID (positive for private chats, negative for group chats)  
If photo received, replies with photo's file_id (for the largest size possible)
Also if you send Audio files, photos, videos and stickers, via private message (or granting it access to messages in groups) it returns you the corresponding file id of each file.
